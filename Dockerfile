ARG TAG
FROM zlutystrop/jekyll-mze-base:$TAG

ARG JEKYLL_SITE_SRC

COPY --chmod=0644 certs/zezulka.dev.pem /etc/ssl/certs/
COPY --chmod=0644 certs/cloudflare.pem /etc/ssl/certs/

# copy generated jekyll artifacts which reside on the top of this dockerfile context
COPY --chown=www-data ${JEKYLL_SITE_SRC} /var/www/zezulka.dev/html

RUN ln -s /etc/nginx/sites-available/zezulka.dev.conf /etc/nginx/sites-enabled/zezulka.dev.conf
RUN update-ca-certificates
