JEKYLL_SITE_SRC=_site
DEV_IMAGE_NAME=jekyll-mze-dev
BASE_IMAGE_NAME=jekyll-mze-base
IMAGE_NAME=jekyll-mze
DOCKERHUB_USER=zlutystrop


local:
	sudo bundle exec jekyll serve -s src -d ${JEKYLL_SITE_SRC}

build-base:
	mkdir -p ${JEKYLL_SITE_SRC} && \
		sudo bundle exec jekyll build -s src -d ${JEKYLL_SITE_SRC} && \
		docker build -t ${BASE_IMAGE_NAME} -f Dockerfile.base .

build-local: build-base
	# will deploy the app only to localhost, HTTP only, i.e. without certs
	docker build --build-arg JEKYLL_SITE_SRC="${JEKYLL_SITE_SRC}" --no-cache -t ${DEV_IMAGE_NAME} -f Dockerfile.dev .

build-public-multiarch:
    # docker buildx create --use --name multi-arch-builder
	docker login -u $(DOCKERHUB_USER) && \
		docker buildx build -f Dockerfile.base . --builder multi-arch-builder --push --platform linux/amd64,linux/arm64 --tag $(DOCKERHUB_USER)/$(BASE_IMAGE_NAME):$(TAG) && \
		docker buildx build --build-arg JEKYLL_SITE_SRC="${JEKYLL_SITE_SRC}" --build-arg TAG=${TAG} . --builder multi-arch-builder --push --platform linux/amd64,linux/arm64 --tag $(DOCKERHUB_USER)/$(IMAGE_NAME):$(TAG)

local-docker: build-local
	docker run -it -p 80:80 --rm ${DEV_IMAGE_NAME}

local-docker-bash: build-local
	docker run -it -p 80:80 --rm ${DEV_IMAGE_NAME} bash

# not intended for real use, I put it here in case Pi bash history is lost :))
install-remotely:
	sudo docker run -p 80:80 -v /home/mzezulka/private.key.pem:/etc/ssl/private/zezulka.dev.pem docker.io/zlutystrop/jekyll-mze:1.8.0 &
