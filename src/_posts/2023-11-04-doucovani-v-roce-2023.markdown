---
layout: post
title: Doučování v roce 2023
date: '2023-11-04 16:27:02 +0100'
date_gmt: '2023-11-04 15:27:02 +0100'
tags:
- maths
---

Hlásím se po roční odmlce. V doučování s menší či větší intenzitou pokračuji, aktuálně mám odučeno 278 hodin a mám v úmyslu pokračovat dál. Začal jsem doučovat i programování, ale dlouhodobě se ho moc držet nechci, zabírá neskutečné množství času.

Čím člověk dělá určité povolání déle, tím větší nadhled v oboru získává, to není převratná myšlenka; tato větší perspektiva z pohledu lektora je ale trochu specifická. Hlavně v tom, že jsem teď v roli učitele a vidím, jak si žáci prochází těmi stejnými trápeními (nemyslím teď primárně v lásce, ale ve škole) co my.

Moje trápení (to školní) bylo minimalizované tím, že matematika mi jako klukovi šla a v hodinách matematiky jsem se spíš nudil; ostatní předměty mi byly celkem jedno a přišla mi vždycky jako velká škoda, že lásku k ní si nevybudoval skoro nikdo další v kolektivu.

Vidím totiž jeden společný motiv během mých doučování, a to je despekt k samotnému předmětu, dost často spojený s antipatií vůči vyučujícím. Tento despekt je - aspoň z toho, co jsem navnímal od studentů - úplně stejný jako ten, který jsem vnímal jako žák u ostatních vrstevníků.

_Kdy tohle proboha někdy použiju v životě?_

_Kdo tohle proboha vymýšlel?_

_Chci hlavně projít ročníkem / udělat přijímačky a pak chci s klidem všechno zapomenout._

Zrovna včera, když jsem skončil doučování, mi jedna žákyně přiznala, že na doučování s jejím učitelem matematiky vlastně ani nepomyslela, protože se ho bojí ("je divnej").

O to víc mi dělá radost, když v některých studentech alespoň trochu vzbudím o předmět zájem, byť je to drtivá menšina z nich.
