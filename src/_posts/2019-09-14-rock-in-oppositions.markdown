---
layout: post
title: Rock In Opposition(s)
date: '2019-09-14 12:49:43 +0200'
date_gmt: '2019-09-14 10:49:43 +0200'
tags:
- music
---

Disclaimer: this is in no way an objective article on RIO[^1], just a collection of random thoughts which occured to me (read as "I found on the Internet, am too much lazy to find and list all the resouces, so I proudly proclaim them as mine instead").

Rock in opposition was formally a very short-lived movement which lasted for only two years (1978-1979). However, the impact RIO has had is immense and far more reaching than those two years. Fortunately enough, [Recommended Records (RēR)](https://www.rermegacorp.com/), which was part of the same impulse as RIO, continues to this day with the practical work of finding and distributing music outside the recognised genres and more importantly (to me, anyway), outside the formal music industry structures. RIO was about people and local to Europe. RēR is a natural extension to the philosophy of RIO and about propagation of the music (all around the world, basically).


> We slowly learned that to consolidate our position we would have to set up our own distribution and information networks. And though our coming together for mutual support focused in RIO, the structure that survived and grew was ReR, because RIO was more turning point in thinking than an enduring workhorse.[^3]

The way I see it, the main message of the RIO movement was this: _there is plenty happening and you can't depend on the music press or the record companies to find it. You have (to start) to look in places that are not so obvious and also don't complain how bad is everything and wait for someone else to do the work, just get on and do it yourself_.


In his book _File Under Popular: Theoretical and Critical Writings on Music_, Chris Cutler[^2] says that _"[Henry Cow] cut loose from agencies and managers and stopped looking for approval from the music press and the critics"_. Their standpoint was practically an opposition (and antidote) to the "authentic" black music white people in the UK and the USA popularized and commercialized but most importantly, only imitated. RIO was an opposition to the "commercial rock circuit", as Cutler bluntly puts it.


I also somewhat agree with the sentiment Cutler conveys when remembering in retrospective what their initial intentions and motives were: naive, arrogant and perhaps very bold. They wanted to create something which makes audience doubt products of the then music industry and make them at least stray away from the mental model "if we can't get  the real thing, we want the closest imitation possible". That's the whole point of avant garde and maybe progressive music, too: to step forward and explore further possibilities which haven't been discovered yet but at the same time which would _a_ certain audience identify itself with.

To finish off, here's a list of random albums which are connected or at least remotely relevant to the RIO movement (groups in bold are the founding members of RIO, not all of them are listed here):

- __Univers Zero__ - Ceux du dehors
- __Samla Mammas Manna__ - Måltid
- __Art Zoyd__ - Symphonie Pour le Jour Ou Bruleront les Cites
- __Henry Cow__ - Unrest
- Present - Triskaïdékaphobie
- U Totem - U Totem
- Magma - Mekanïk Destruktïw Kommandöh
- The Residents - The Third Reich 'N' Roll
- Faust - Faust
- Thinking Plague - In Extremis
- Negativland - Escape from Noise
- Captain Beefheart - Trout Mask Replica

---
[^1]: Further information available at the website of [Squidco](http://www.squidco.com/rer/RIO.html)
[^2]: Chris Cutler is a centerpiece figure in the RIO movement, it might be a good idea to make a research about him (or buy his book!) if you want to dig deeper
[^3]: 20 years of Rock in Opposition, Interview with Chris Cutler (1998), source: http://www.mitkadem.co.il/RIO_interview.html

