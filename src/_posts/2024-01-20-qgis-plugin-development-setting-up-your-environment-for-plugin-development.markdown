---
layout: post
title: "2. Setting Up Your Environment for Plugin Development"
date: '2024-01-20 17:27:23 +0100'
date_gmt: '2024-01-20 16:27:23 +0100'
tags:
- qgis
- gis
- python
permalink: /qgis-plugin-development/2
---

In this series, we will use Python as our main programming language and `git` as the version control system.

There are a few things we need to install and set up before we can comfortably work on our first plugin. These are the main tools we will use:
- Python 3 + Python IDE
- QT Designer
- Docker
- QGIS

### 1. Python 3 + Python IDE

I've used the [community edition of PyCharm](https://www.jetbrains.com/pycharm/) but this choice is really up to you. The feature I've used the most was a very intuitive test execution directly from the IDE and solid Makefile support. I'm mainly mentioning this because a) it's good to know there is such a thing as IDE and b) you don't necessarily need anything fancy compared to what you're used to, even a simple text editor will be enough for a simple plugin.

### 2. QT Designer

This one was a lifesaver for me. During development of a more sophisticated plugin, you will need to design a __user interface__. I'm personally not a big fan of WYSIWYG editors but let's just say that user interface design is not one of my strengths so any help counts. :)

In the end, all you need to pass in to QGIS is a specialized subset of an XML similar to this:

{% highlight xml %}

<?xml version="1.0" encoding="UTF-8"?>
<ui version="4.0">
 <class>Frame</class>
 <widget class="QFrame" name="Frame">
  <property name="windowTitle">
   <string>Frame</string>
  </property>
  <layout class="QGridLayout" name="gridLayout_2">
   <item row="0" column="0">
    <layout class="QGridLayout" name="gridLayout">
     <item row="1" column="0">
      <widget class="QProgressBar" name="progressBar">
       <property name="value">
        <number>24</number>
       </property>
      </widget>
     </item>
     <item row="0" column="0">
      <widget class="QTextBrowser" name="textBrowser"/>
     </item>
    </layout>
   </item>
  </layout>
 </widget>
 <resources/>
 <connections/>
</ui>

{% endhighlight %}

but seeing what you're really designing can be quite helpful. With QT Designer, I was easily able to sketch up plugin toolbars and other QGIS interface components, like so:

![QT designer](/assets/images/qgis-plugin/qt-designer.png)
*QT Designer in action. I've used the version 5.15.3 most of the time.*

### 3. Testing

Apart from the plugin itself, unit and integration tests should be a crucial part of your plugin. Testing your code is at least important as writing the code itself. Why? There are many reasons:
- a good quality test suite is in many cases the __best documentation__ you can possibly write for your code because tests by definition always reflect the current state of functionality (compare this to textual documentation which - to some extent, at least - we cannot validate)
- with any small change, you __don't need to start QGIS__ and manually test that it _somehow works™_ - you'll just run your tests!
- writing tests forces you to __use the code as a consumer__ which in many cases leads to improving code readability and maintainability - this is especially important when you'll be working on the plugin in a team
- you will be substantially less afraid to __make any changes__ to your code in the future because if you keep on writing good quality tests, those tests will always back you up and point out flaws in any of your changes

There are two types of tests you should be writing for your QGIS plugin: __unit__ tests and __integration__ tests. If your QGIS plugin was a car assembled in a factory, unit testing would be checking that individual car parts are okay by themselves (=we can start the engine in an isolated environment without putting it into the car), whereas integration testing would be checking that the car works as a whole once it's assembled. During integration testing, we really don't care whether the engine works because we're already scrutinizing individual parts by unit testing - we're rather making sure that the _car as a whole_ works as expected.

Coming back to our toy QGIS Geoapify plugin, unit tests will consist of assertions and checks of our plugin units, e.g. that the HTTP Geoapify client works properly under various circumstances (including faulty behaviour!). Integration tests, on the other hand, will test all our plugin code as a whole. We will use Docker to make sure we can run our plugin on any QGIS, not only on [our laptop](https://xkcd.com/1806/).

To keep this article short, I will not dwell into what Docker is in case you don't know; check [other sources](https://www.docker.com/resources/what-container/) for a more detailed explanation. For our intents and purposes, though, Docker is a tool which enables us to automatically run integration tests on a fresh environment, free of configurations and setups of our machine.

### 4. QGIS

Theoretically, you could develop your plugin without any standalone QGIS installation on your computer; in reality, especially when you're prototyping things, installing your plugin into the standalone QGIS is very helpful. __Make sure you clearly communicate what QGIS version(s) your plugin supports.__

We're now ready to [start coding](/qgis-plugin-development/3.html).
