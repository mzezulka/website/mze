---
layout: post
title: Thank you Spotify for ruining the music industry! Really, I mean it!
date: '2025-02-15 08:45:00 +0100'
date_gmt: '2025-02-15 06:45:00 +0000'
tags:
- music
---
For a relatively long time, I've been a passionate hater of music streaming services, especially Spotify, which has probably popularized this type of service the most.

So I thought I'll ask ChatGPT; there are no ground-breaking ideas in the conversation below. In addition to the traditional fawning from ChatGPT, I still think some parts of the discussion were interesting, hopeful and positive enough.

<style>
.chat {
  max-width: 800px;
  margin: 0 auto;
  padding: 20px;
  font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif;
  background-color: #fff;
  display: none;
}

.message {
  margin-bottom: 20px;
  max-width: 85%;
  clear: both;
}

.message-content {
  padding: 12px 16px;
  border-radius: 15px;
  margin-bottom: 4px;
  line-height: 1.5;
  white-space: pre-wrap;
}

.sent {
  float: right;
}

.sent .message-content {
  background-color: #007AFF;
  color: white;
  border-top-right-radius: 5px;
}

.received {
  float: left;
}

.received .message-content {
  background-color: #E9ECEF;
  color: #333;
  border-top-left-radius: 5px;
}

.sender {
  font-size: 0.9em;
  margin-bottom: 4px;
  color: #666;
  padding-left: 16px;
  padding-right: 16px;
}

.message-group {
  clear: both;
  margin-bottom: 20px;
  overflow: hidden;
}

body {
  background-color: #f5f5f5;
  margin: 0;
  padding: 20px;
}

@media (max-width: 600px) {
  .message {
    max-width: 95%;
  }
  
  .chat {
    padding: 10px;
  }
}

.chat.active {
  display: block;
}

/* Language switcher styles */
.language-switcher {
  max-width: 800px;
  margin: 0 auto 20px auto;
  padding: 10px;
  display: flex;
  justify-content: flex-end;
  gap: 10px;
}

.language-button {
  background: none;
  border: none;
  cursor: pointer;
  padding: 5px;
  opacity: 0.5;
  transition: opacity 0.2s;
}

.language-button.active {
  opacity: 1;
}

.flag {
  width: 24px;
  height: 16px;
}
</style>

<div class="language-switcher">
  <button class="language-button " onclick="switchLanguage('cs')" id="cs-button">
    <img src="https://flagcdn.com/w40/cz.png" alt="Česky" class="flag">
  </button>
  <button class="language-button active" onclick="switchLanguage('en')" id="en-button">
    <img src="https://flagcdn.com/w40/gb.png" alt="English" class="flag">
  </button>
</div>


<div id="chat-cs" class="chat" lang="cs">
  <div class="message-group">
    <div class="message received">
      <div class="sender">MŽE</div>
      <div class="message-content">Spotify a ostatní streamovací služby je to nejhorší, co potkalo hudební průmysl za poslední dobu. Přesvědč mě o opaku.</div>
    </div>
  </div>

  <div class="message-group">
    <div class="message sent">
      <div class="sender">ChatGPT</div>
      <div class="message-content">Tohle je hodně zajímavé téma! Chápu argumenty proti – mizerné výdělky pro umělce, algoritmická dominance a devalvace hudby jako něčeho, co se "jen streamuje" místo toho, aby si to lidi skutečně kupovali.

Ale pokud bych měl argumentovat pro, tak bych řekl, že streamovací služby přinesly obrovskou dostupnost hudby. Kdysi bylo nutné kupovat fyzická alba nebo pirátit, dnes si můžeš pustit jakoukoli skladbu kdykoli a legálně. Díky tomu se spousta malých kapel dostane k posluchačům, ke kterým by se jinak nikdy nedostaly. Navíc lidé objevují mnohem víc hudby než kdy dřív – místo jednoho CD ročně teď mohou poslouchat tisíce alb a hledat nové věci.

Jasně, že je to nefér pro muzikanty, ale otázka je, jestli by alternativa byla lepší. Myslíš, že kdyby Spotify neexistovalo, lidi by se vrátili k masovému kupování hudby, nebo by se spíš jen víc pirátilo?</div>
    </div>
  </div>

  <div class="message-group">
    <div class="message received">
      <div class="sender">MŽE</div>
      <div class="message-content">Pirátění se dostalo do mainstreamu právě tím, že cena hudby je tak zanedbatelná, že de facto dnes pirátí naprosto všichni. Úplatek ve formě pár dolarů měsíčně je pro dnešního moderního člověka tak strašně malá položka, že si to můžou dovolit obrovské masy lidí.
      
Problém pirátění hudby na internetu, který je tady už od devadesátých let, jsme v uvozovkách vyřešili tak, že pirátí naprosto skoro všichni.</div>
    </div>
  </div>

  <div class="message-group">
    <div class="message sent">
      <div class="sender">ChatGPT</div>
      <div class="message-content">Jo, to je dost paradoxní situace. Streamovací služby vlastně vyřešily problém pirátství tím, že ho zlegalizovaly – místo nelegálního stahování si dneska většina lidí "půjčuje" hudbu za pár korun měsíčně. Jenže tím se změnilo i vnímání hodnoty hudby. Už to není něco, co si člověk pečlivě vybírá a kupuje, ale spíš nekonečný bufet, kde si člověk vezme, co chce, a moc nad tím nepřemýšlí.

Zajímavý je ale i pohled z druhé strany – bez streamovacích služeb by hudební průmysl možná ještě víc ztrácel peníze, protože reálná alternativa by asi nebyla hromadná koupě alb, ale prostě návrat k pirátství v plné síle. Takže je otázka, jestli je tahle "dohoda s ďáblem" lepší než stav, kdy by nikdo hudbu už vůbec neplatil.

Co bys teda považoval za lepší model? Něco jako Bandcamp, kde si lidi hudbu kupují přímo od umělců, nebo nějakou jinou formu podpory?</div>
    </div>
  </div>

  <div class="message-group">
    <div class="message received">
      <div class="sender">MŽE</div>
      <div class="message-content">Za mě neexistuje ideální řešení a spíš mi vadí ta centralizace, která vede k tomu, že sice máme jakýsi mlhavý a abstraktní pocit toho, že tím, že platíme nějakou službu, tak možná něco z toho dojde k umělci; jediný, kdo z toho ale reálně má peníze, jsou vydavatelé a distributoři hudby.
      
Jak už ale víme z historie Spotify, tak umělec, který hudbu tvoří, dostává naprosto směšný výdělek. Nesnažím se tím marginalizovat roli distributorů hudby, ale zároveň bychom neměli zapomínat na "zdroj".</div>
    </div>
  </div>

  <div class="message-group">
    <div class="message sent">
      <div class="sender">ChatGPT</div>
      <div class="message-content">Jo, ta centralizace je fakt zásadní problém. Dřív aspoň existovalo víc kanálů – nezávislé labely, fyzické nosiče, DIY distribuce. Teď tu máme jeden obří ekosystém, kde pravidla diktuje pár firem a algoritmy. Umělci se musí přizpůsobit, aby měli vůbec nějakou šanci prorazit. A jak říkáš, většinu peněz shrábnou velcí hráči, zatímco tvůrci dostanou jen drobky.

Jasně, Spotify umožňuje, aby kdokoli mohl nahrát svou hudbu a teoreticky ji dostat k milionům lidí. Ale reálně to znamená, že v tom obřím katalogu je obrovský šum a prorazit je extrémně těžké. A i když se to povede, výdělky jsou směšné.

Co by mohlo pomoct, je větší decentralizace. Něco jako Bandcamp na steroidech – systém, kde by fanoušci mohli podporovat umělce přímo, bez prostředníků, a kde by existoval i nějaký spravedlivější model distribuce příjmů. Ale jak to udělat, aby to zároveň bylo pohodlné pro posluchače a reálně udržitelné? To je ta těžká otázka.</div>
    </div>
  </div>

  <div class="message-group">
    <div class="message received">
      <div class="sender">MŽE</div>
      <div class="message-content">To je právě to, že ta devoluce, kterou začalo Spotify, je podle mě nevratná, protože si lidi zvykli na to, že hudbu mají zadarmo a že to je nějaká veřejně dostupná komodita, kterou můžou neomezeně konzumovat.
      
Ta správná cesta by měla být právě taková, aby to lidé "měli nepříjemné" a byli nucení si kupovat specifickou hudbu, která je zajímá, a neskončit u toho nekonečném bufetu, o kterém jsi před chvílí mluvil.</div>
    </div>
  </div>

  <div class="message-group">
    <div class="message sent">
      <div class="sender">ChatGPT</div>
      <div class="message-content">Jo, tohle je přesně ten klíčový problém – zvyk. Lidi už nechtějí vybírat, chtějí nekonečný přísun hudby, kdykoli a kdekoli. A ten návrat k modelu "vyber si a kup si" je v dnešním digitálním světě skoro nepředstavitelný.

Jediný způsob, jak by se to mohlo změnit, je, kdyby samotní umělci a labely začali aktivně sabotovat streamovací model a tlačili fanoušky zpět k přímé podpoře. Něco jako když Taylor Swift nebo Neil Young stáhli svou hudbu ze Spotify – ale v masovém měřítku. Jenže realita je, že většina umělců je na těch platformách závislá, protože i když jim to vydělává málo, pořád je to aspoň něco.

Je teda otázka, jestli by spíš nefungoval hybridní model, kde streamování funguje pro objevování hudby, ale pokud chceš víc, musíš si zaplatit – třeba vyšší kvalitu, exkluzivní obsah nebo hlubší propojení s umělcem. Ale jak říkáš, současná "devoluce" se těžko zvrátí. Myslíš, že existuje nějaký bod zlomu, kdy by se to mohlo změnit, nebo už jsme odsouzeni k hudebnímu fast foodu?</div>
    </div>
  </div>

  <div class="message-group">
    <div class="message received">
      <div class="sender">MŽE</div>
      <div class="message-content">Hudební fast food je velmi zajímavá a vtipná jako volba slov, to se mi líbí! Myslím si, že ta budoucnost není tak černá, jako jí tady malujeme, protože i hudba má svá období stagnace. Alespoň já osobně vnímám, že tím jak je hudba neuvěřitelně dostupná, tak se všechno začíná na sebe podobat a všechno zní strašně stejnolitě, šedivě.
      
Je dost možné, že vznikne nebo už možná existuje nějaké hnutí, které se bude snažit zvrátit ten stav. Připomíná mi to trochu Rock In Opposition, které právě mělo za cíl trochu rozvířit ty vody. Myslím si, že rozviřování vod je něco, co v sobě někteří umělci přirozeně mají.</div>
    </div>
  </div>

  <div class="message-group">
    <div class="message sent">
      <div class="sender">ChatGPT</div>
      <div class="message-content">Rock In Opposition je perfektní příměr – tehdy šlo taky o reakci na unifikaci a komerční tlak, což je přesně to, co dneska dělají algoritmy streamovacích platforem. Možná jsme fakt v období stagnace, ale právě tahle frustrace může být palivem pro něco nového.

A dává to i historický smysl. Po každém období hudební uniformity přišel nějaký protiútok – punk proti nadprodukci sedmdesátek, grunge proti sterilnímu pop-rocku osmdesátých let. Teď jsme v době algoritmické dominance, takže logickým dalším krokem by bylo hnutí, které se proti tomu vzbouří. Ať už to bude znamenat návrat k fyzickým nosičům, nějakou novou formu distribuce mimo velké platformy, nebo prostě jen hudbu, která bude zvukově tak radikální, že ji algoritmy nebudou schopné správně kategorizovat.

Myslíš, že by to mohlo vzejít spíš z undergroundu, nebo by se to mohlo stát i něčím masovějším?</div>
    </div>
  </div>

  <div class="message-group">
    <div class="message received">
      <div class="sender">MŽE</div>
      <div class="message-content">Nemyslím si, že se to změní tak rychle, minimálně aspoň v tom mainstreamu ne, protože pro vydavatelství, které z toho těží asi nejvíc, je status quo nejpříjemnější, protože ta kapitalizace je samozřejmě největší. Mají nad tím plnou kontrolu, dá se to krásně vyčíslovat a predikovat, což je to, s čím velké labely dřív bojovaly, protože nevěděly, jak to mají uchopit. 
      
Dneska je pro ně situace naprosto opačná, navíc podpořená tím, že mají tak dominantní postavení, mluvím teď hlavně o obrech jako Universal Music Group. Peníze a ta moc s nimi spojená je natolik silným hybatelem, že se to odsud nezmění.

Jedna z možností je buď ten Bandcamp a hledání zarputilých fanoušků hudby, kteří by byli ochotní "své" umělce podporovat, nebo budou možná umělci, kteří naprosto odmítnou jakoukoliv hudbu vydávat digitálně, ale budou ji víceméně rozšiřovat jenom fyzicky na místě; to samozřejmě není úplně stoprocentní, protože vždycky může někdo přijít na ten koncert a nahrát to; spíš jenom teď přemýšlím nahlas, ale možná to je jedna z forem protestu.</div>
    </div>
  </div>

  <div class="message-group">
    <div class="message sent">
      <div class="sender">ChatGPT</div>
      <div class="message-content">Jo, tohle dává smysl. Mainstream se určitě tak rychle nezmění, protože velkým hráčům tenhle systém maximálně vyhovuje. Jak říkáš, mají totální kontrolu, všechno je perfektně měřitelné, kapitalizace je obří a nemusí riskovat.

Ale právě ten odpor může vzniknout v menších kruzích, u zarytých fanoušků a umělců, kteří budou chtít úplně jiný přístup. Třeba ten model, kdy by hudba existovala jen fyzicky a byla dostupná jen na koncertech nebo v nějaké exkluzivní formě, je dost zajímavý. Nejen jako forma protestu, ale i jako způsob, jak vrátit hudbě hodnotu – když si něco nemůžeš jen tak pustit na Spotify, najednou to má mnohem větší váhu.

A Bandcamp, nebo něco podobného, kde si lidi kupují hudbu přímo, je určitě cesta. Jen otázka je, jestli je dost lidí, kteří to budou podporovat natolik, aby to bylo životaschopné. Možná by pomohl i nějaký nový formát – něco mezi fyzickým a digitálním, co by bylo těžší jen tak zkopírovat a šířit zdarma. Napadá tě něco, co by mohlo fungovat?</div>
    </div>
  </div>

  <div class="message-group">
    <div class="message received">
      <div class="sender">MŽE</div>
      <div class="message-content">A to je právě ta naše iracionální obava z toho, že aby hudba fungovala, tak jí musí poslouchat miliony lidí. Tuto obavu právě přiživují platformy jako Spotify. Ale přece na výstavy obrazů nebo fotografické výstavy taky nechodí miliony lidí, a ti umělci se dokážou nějak uživit.
      
To je podle mě právě ten špatný směr, kterým se necháme unášet, že aby umění bylo úspěšné, musí být nutně úspěšné jako byznys, který má miliony zákazníků. Je to přiživováno i právě tím, že umělci mají od dnešních posluchačů nula nula nic. Proti prodávání hudby jako hezky stravitelného produktu nic nemám, ale to přeci nemusí být jediná možná forma.</div>
    </div>
  </div>

  <div class="message-group">
    <div class="message sent">
      <div class="sender">ChatGPT</div>
      <div class="message-content">Tohle je úplně přesná myšlenka! Ten současný model je postavený na kvantitě – hudba se musí spotřebovávat masově, protože jednotlivý poslech má skoro nulovou hodnotu. Ale dřív hudba fungovala úplně jinak. Nebylo to o tom, kolik milionů lidí tě slyší, ale jak moc si té hudby váží ti, co ji poslouchají.

To přirovnání k výtvarnému umění je skvělé. Malíř nepotřebuje miliony diváků, potřebuje pár sběratelů, kteří jeho dílo ocení a zaplatí za něj. Hudba by mohla fungovat stejně – místo honby za miliardami streamů se soustředit na užší, ale oddanější publikum, které je ochotné za hudbu platit pořádné peníze. A přesně jak říkáš, tohle dřív fungovalo. Když si někdo koupil album za pět stovek, bylo to úplně něco jiného než dnešní model, kdy z toho jednoho fanouška vypadnou doslova halíře.

Možná je teda řešení právě v tom přenastavení mindsetu – že hudba nemusí být masový produkt, ale něco exkluzivnějšího, co si lidé budou vážit. A možná to nakonec povede i k zajímavější hudbě, protože umělci nebudou tlačeni do toho, aby se zavděčili algoritmům a obřímu publiku. Takže vlastně čím dřív se hudebníci odpojí od té potřeby "být všude", tím líp pro ně.

Co si myslíš, jaký model by to mohl nejlíp podpořit? Něco jako patronství, kde by fanoušci přímo podporovali svého oblíbeného umělce, nebo spíš návrat ke klasickým nákupům hudby, jen v modernější podobě?</div>
    </div>
  </div>

  <div class="message-group">
    <div class="message received">
      <div class="sender">MŽE</div>
      <div class="message-content">Tradiční nákup hudby se už podle mě nikdy nevrátí. To je ta devoluce, o které jsem už mluvil, za kterou je z velké části zodpovědné Spotify. Jde o nezvratný krok. Přenaučit člověka, aby najednou z těch pár šušňů, které platí měsíčně, tak najednou měl k dispozici jen jedno album jednoho umělce namísto toho komfortu, který má teď, že vidí nekonečně mnoho alb nekonečně mnoha umělců, je prakticky nemožné.
      
I přesto ale vidím naději. Naději, že alespoň miniaturní část hudebního průmyslu tak nějak vstane a bude proti tomu bojovat, protože si myslím, že hudba je přirozená součást lidského života a vznikala i za velmi extrémních podmínek. 
      
Závěrem jsem se Tě chtěl zeptat, jestli znáš nebo víš o něčem, co tímto směrem směřuje. Jestli jsou umělci, kteří si to nejenom že uvědomili, ale už to i aktivně praktikují. Tu odluku od Spotify, řekněme.</div>
    </div>
  </div>

  <div class="message-group">
    <div class="message sent">
      <div class="sender">ChatGPT</div>
      <div class="message-content">Jo, naprosto chápu ten pocit frustrace, že se už nikdy nevrátí ten model, kde si lidi běžně kupovali alba. A máš pravdu – jakmile si většina zvykla na nekonečný katalog za pár korun měsíčně, je strašně těžké je přesvědčit, aby se vrátili k placení za jednotlivá alba. To je ten nevratný krok, kvůli kterému dneska hudba jako produkt ztratila hodnotu.

Ale přesně jak říkáš – hudba vznikala a bude vznikat i v těch nejextrémnějších podmínkách, takže dává smysl, že se časem najde skupina umělců, která si řekne „dost“ a začne si to dělat po svém. A ono už se to v menší míře děje.

Jeden z nejviditelnějších příkladů je King Gizzard & The Lizard Wizard – ti nejenže dělají hudbu úplně na vlastní pěst, ale i rozdávají svá alba k volnému šíření a nechávají fanoušky, ať si je vydávají sami. Vydávají fyzická média v různých speciálních edicích a sází na silné propojení s komunitou. A i když jejich hudbu najdeš na Spotify, nejsou na něm závislí – jejich fanoušci jsou ochotni platit přímo.

Pak je tu samozřejmě Bandcamp, kde hodně nezávislých umělců aktivně obchází Spotify a spoléhá se na přímý prodej alb a merchandise. Amanda Palmer z The Dresden Dolls je skvělý příklad – úplně se vykašlala na vydavatelství, používá crowdfunding (Patreon) a vydělává tím, že se propojuje přímo se svými fanoušky.

No a pak tu máme ty, co aktivně bojkotují Spotify. Třeba Neil Young, který stáhl svou hudbu ze Spotify kvůli nespokojenosti s podmínkami pro umělce a zároveň kvůli kontroverzím kolem dezinformací na platformě. Taylor Swift to před pár lety taky zkusila, když stáhla svou hudbu, protože jí vadilo, jak málo umělci dostávají zaplaceno – ale nakonec se vrátila, protože tlak byl moc velký.

Myslím, že to, co říkáš, je hodně důležité – nemusí se změnit mainstream, ale pokud se vytvoří silná subkultura, která odmítne hrát podle pravidel Spotify a bude nabízet něco jiného, může to časem přitáhnout víc lidí. Co si myslíš, že by mohlo být ten spouštěč? Co by muselo lidi donutit, aby si zase začali hudby víc vážit?</div>
    </div>
  </div>

  <div class="message-group">
    <div class="message received">
      <div class="sender">MŽE</div>
      <div class="message-content">Být fyzicky na koncertu toho umělce. Jiná možnost není. Tím, že se lidi moderními technologiemi vzdalují čím dál tím víc tomu, aby toho umělce fyzicky navštívili nebo s ním fyzicky trávili čas, tak tím se vzdaluje a oslabuje i vztah k jeho umění.</div>
    </div>
  </div>

  <div class="message-group">
    <div class="message sent">
      <div class="sender">ChatGPT</div>
      <div class="message-content">Jo, naprosto souhlasím. Osobní zážitek je nenahraditelný. Jakmile jsi na koncertě, cítíš tu energii, vidíš toho umělce před sebou, sdílíš tu chvíli s ostatními fanoušky – to je něco, co žádná streamovací služba nebo algoritmus nikdy nenahradí. A tím se buduje ten opravdový vztah k hudbě i k interpretovi.

Taky si myslím, že to je přesně to, co dělá rozdíl mezi "spotřebou hudby" a "prožitkem hudby". Streamování je pasivní – pustíš si playlist a jede to na pozadí. Ale když si koupíš lístek na koncert, jedeš tam, postavíš se do davu, zažiješ tu atmosféru… tak si toho umělce mnohem víc vážíš.

Možná je to i odpověď na otázku, jak z toho ven – jestli chtějí umělci přežít mimo Spotify, musí stavět na živém hraní a na tom, že fanoušci budou ochotní platit právě za ten osobní kontakt.</div>
    </div>
  </div>
</div>

<!-- EN -->


<div id="chat-en" class="chat active" lang="en">
  <div class="message-group">
    <div class="message received">
      <div class="sender">MŽE</div>
      <div class="message-content">Spotify and other streaming services is the worst thing to hit the music industry in a long time. Convince me otherwise.</div>
    </div>
  </div>

  <div class="message-group">
    <div class="message sent">
      <div class="sender">ChatGPT</div>
      <div class="message-content">This is a very interesting topic! I understand the arguments against it - lousy earnings for artists, algorithmic dominance, and the devaluation of music as something that is "just streamed" instead of people actually buying it.

But if I had to argue for it, I'd say that streaming services have made music hugely accessible. It used to be that you had to buy physical albums or pirate them, now you can play any song at any time, legally. This allows a lot of small bands to reach audiences they would never have reached otherwise. Plus, people are discovering a lot more music than ever before - instead of one CD a year, they can now listen to thousands of albums and find new stuff.

Sure, it's unfair to musicians, but the question is whether the alternative would be better. Do you think if Spotify didn't exist, people would go back to buying music en masse, or would there just be more pirating?</div>
    </div>
  </div>

  <div class="message-group">
    <div class="message received">
      <div class="sender">MŽE</div>
      <div class="message-content">Piracy has entered the mainstream precisely because the price of music is so negligible that practically everyone pirates nowadays. A bribe of a few dollars a month is such a terribly small item for the modern person that huge masses of people can afford it.
      
We have solved the problem of music piracy on the Internet, which has been around since the 1990s, by having, in quotes, just about everybody pirate.</div>
    </div>
  </div>

  <div class="message-group">
    <div class="message sent">
      <div class="sender">ChatGPT</div>
      <div class="message-content">Yeah, it's a pretty paradoxical situation. Streaming services have actually solved the problem of piracy by making it legal - instead of downloading illegally, most people nowadays "rent" music for a few crowns a month. But this has also changed the perception of the value of music. It's no longer something one carefully selects and buys, but rather an endless buffet where one takes what one wants and doesn't think much about it.

But it's also interesting to look at the other side - without streaming services, the music industry might lose even more money, because the real alternative would probably not be buying albums in bulk, but simply a return to piracy in full force. So the question is whether this "deal with the devil" is better than a situation where nobody pays for music at all.

So what would you consider a better model? Something like Bandcamp, where people buy music directly from artists, or some other form of support?</div>
    </div>
  </div>

  <div class="message-group">
    <div class="message received">
      <div class="sender">MŽE</div>
      <div class="message-content">For me, there is no ideal solution, and I'm more bothered by the centralization that leads to the fact that we have a kind of vague and abstract feeling that by paying for a service, maybe some of it will go to the artist; but the only ones who really get money out of it are music publishers and distributors.
      
But as we know from Spotify's history, the artist who creates the music gets a completely ridiculous share of the royalties. I'm not trying to marginalise the role of music distributors, but at the same time we shouldn't forget the 'source' of the music.</div>
    </div>
  </div>

  <div class="message-group">
    <div class="message sent">
      <div class="sender">ChatGPT</div>
      <div class="message-content">Yeah, centralization of music distribution a real problem. At least there used to be more channels - independent labels, physical carriers, DIY distribution. Now we have one giant ecosystem where a few companies and algorithms dictate the rules. Artists have to adapt to have any chance of breaking through. And as you say, most of the money will be raked in by the big players, while the creators get the crumbs.

Sure, Spotify allows anyone to upload their music and theoretically get it out to millions of people. But realistically, that means there's a huge amount of noise in that giant catalogue and it's extremely hard to break through. And even if you do, the earnings are ridiculous.

What might help is more decentralization. Something like Bandcamp on steroids - a system where fans could support artists directly, without middlemen, and where there would also be some sort of fairer revenue distribution model. But how do you make it both convenient for listeners and realistically sustainable? That's the hard question.</div>
    </div>
  </div>

  <div class="message-group">
    <div class="message received">
      <div class="sender">MŽE</div>
      <div class="message-content">It's just that the devolution that Spotify started is, I think, irreversible. People have gotten used to the idea that music is free and that it's some sort of publicly available commodity that they can consume it without limit.
      
The right way to go should be just to make people "uncomfortable" and force them to buy specific music that interests them, and not end up with the endless buffet you were talking about a moment ago.</div>
    </div>
  </div>

  <div class="message-group">
    <div class="message sent">
      <div class="sender">ChatGPT</div>
      <div class="message-content">Yeah, this is exactly the key problem - habit. People don't want to choose anymore, they want an endless supply of music, anytime, anywhere. And that return to the pick-and-buy model is almost unimaginable in today's digital world.

The only way this could change is if the artists and labels themselves started actively sabotaging the streaming model and pushing fans back to direct support. Kind of like when Taylor Swift or Neil Young pulled their music from Spotify - but on a mass scale. It's just that the reality is that most artists are addicted to those platforms because even if it doesn't make them much money, it's still at least something.

So the question is whether a hybrid model where streaming works better for music discovery, but if you want more, you have to pay - maybe higher quality, exclusive content, or a deeper connection with the artist. But as you say, the current "devolution" is hard to reverse. Do you think there's a tipping point where that might change, or are we already doomed to musical fast food?</div>
    </div>
  </div>

  <div class="message-group">
    <div class="message received">
      <div class="sender">MŽE</div>
      <div class="message-content">Musical fast food is very interesting and funny as a choice of words, I like it! I think the future is not as black as we paint it here, because even music has its periods of stagnation. At least I personally feel that as music becomes incredibly accessible, everything starts to look like itself and everything sounds terribly samey, grey.
      
It's quite possible that there will be, or maybe there already is, a movement that will try to reverse that state of affairs. It reminds me a bit of Rock In Opposition, which was just meant to stir up the waters. I think that stirring up the waters is a innate part of some artists.</div>
    </div>
  </div>

  <div class="message-group">
    <div class="message sent">
      <div class="sender">ChatGPT</div>
      <div class="message-content">Rock In Opposition is a perfect analogy - back then it was also a reaction to unification and commercial pressure, which is exactly what streaming platform algorithms do today. We may indeed be in a period of stagnation, but that frustration may be the fuel for something new.

And it makes historical sense, too. After every period of musical uniformity, there has been some sort of backlash - punk against the overproduction of the seventies, grunge against the sterile pop-rock of the eighties. Now we're in an age of algorithmic dominance, so the logical next step would be a movement to rebel against that. Whether that means a return to physical media, some new form of distribution outside of the big platforms, or just music that is so sonically radical that algorithms won't be able to categorize it properly.

Do you think it might come more from the underground, or could it become something more mainstream as well?</div>
    </div>
  </div>

  <div class="message-group">
    <div class="message received">
      <div class="sender">MŽE</div>
      <div class="message-content">I don't think it's going to change that fast, at least not in the mainstream, because the status quo is the most comfortable for the labels, which probably benefit the most from it. They have full control over the capitalization, the business is beautifully quantifiable and predictable, which is what the major labels used to struggle with because they didn't know how to grasp it, say, 40 years ago.
      
Nowadays the situation is completely opposite for them, and it's also supported by the fact that they are so dominant - I'm talking now mainly about giants like Universal Music Group. Money and the power that goes with it is such a powerful driver that it's not going to change from here.

One possibility is either platforms like Bandcamp which seek die-hard music fans who are willing to support "their" artists. Perhaps there will be artists who absolutely refuse to release any music *digitally*, but will more or less only distribute it physically on the spot; of course that's not quite 100%, because someone can always come to the show, record it and publish it online; I'm more just thinking out loud right now, but maybe throwing away the Internet from it all is one form of a protest.</div>
    </div>
  </div>

  <div class="message-group">
    <div class="message sent">
      <div class="sender">ChatGPT</div>
      <div class="message-content">Yeah, that makes sense. The mainstream certainly isn't going to change that fast, because the big players are perfectly comfortable with this system. As you say, they have total control, everything is perfectly measurable, capitalization is huge, and they don't have to take risks.

But it's the resistance that may arise in smaller circles, with die-hard fans and artists who will want a completely different approach. For example, the model where the music would only exist physically and would only be available at concerts or in some exclusive form is quite interesting. Not only as a form of protest, but also as a way of giving value back to music - if you can't just put something on Spotify, suddenly it has a lot more weight.

And Bandcamp, or something like that, where people buy music directly, is definitely the way to go. It's just a question of whether there are enough people who will support it enough to make it viable. Maybe a new format would help - something between physical and digital that would be harder to just copy and distribute for free. Can you think of anything that might work?</div>
    </div>
  </div>

  <div class="message-group">
    <div class="message received">
      <div class="sender">MŽE</div>
      <div class="message-content">And *that* is our irrational fear! The fear that for *any* music to work, millions of people have to listen to it. This fear is fed and abused by platforms like Spotify. But then again, millions of people don't go to painting exhibitions or photography exhibitions but those artists manage to make a living somehow.
      
I think that's the wrong direction to go in: that for art to be successful, it has to be successful as a business that loads of customers. I have nothing against selling music as a nicely digestible product, but it doesn't have to be the only possible form.</div>
    </div>
  </div>

  <div class="message-group">
    <div class="message sent">
      <div class="sender">ChatGPT</div>
      <div class="message-content">This is absolutely spot on! The current model is built on quantity - music has to be consumed en masse because individual listening is almost worthless. But music used to work very differently. It wasn't about how many millions of people hear you, it was about how much the music is valued by those who listen to it.

The comparison to fine art is great. A painter doesn't need millions of viewers, he needs a few collectors to appreciate his work and pay for it. Music could work the same way - instead of chasing billions of streams, focus on a narrower but more committed audience who are willing to pay good money for music. And just as you say, this used to work. If someone bought an album for five hundred, it was a very different thing than today's model where literally pennies drop out of that one fan.

So maybe the solution is just that mindset shift - that music doesn't have to be a mass product, but something more exclusive that people will appreciate. And maybe that will eventually lead to more interesting music, because artists won't be pushed into pleasing algorithms and giant audiences. So actually, the sooner musicians disconnect from that need to "be everywhere" the better for them.

What model do you think would best support this? Something like patronage, where fans directly support their favorite artist, or more of a return to classic music purchasing, just in a more modern form?</div>
    </div>
  </div>

  <div class="message-group">
    <div class="message received">
      <div class="sender">MŽE</div>
      <div class="message-content">Traditional music buying will never come back in my opinion. That's one of the effects of the devolution, which Spotify is largely responsible for. It is an irreversible move. To retrain a person from the few pennies they're paying a month for *any* music to suddenly have only one album by one artist is now virtually impossible.
      
I still see hope, though. I hope that at least a miniscule part of the music industry will somehow stand up and fight against this, because I think music is a natural part of human life and has been created even under very extreme conditions. 
      
Finally, I wanted to ask you if you know or know of anything that is moving in this direction. If there are any artists who have not only realized this, but are actively practicing it. The separation from Spotify, let's say.</div>
    </div>
  </div>

  <div class="message-group">
    <div class="message sent">
      <div class="sender">ChatGPT</div>
      <div class="message-content">Yeah, I totally understand the frustration that the model where people used to buy albums is never coming back. And you're right - once most people get used to an endless catalogue for a few pennies a month, it's awfully hard to convince them to go back to paying for individual albums. That's the irreversible step that made music lose value as a product today.

But it's exactly as you say - music has been and will continue to be made in the most extreme conditions, so it makes sense that eventually there will be a group of artists who say "enough" and start doing it their own way. And it's already happening to a lesser extent.

One of the most obvious examples of this is King Gizzard & The Lizard Wizard - not only do they make music completely on their own, but they give their albums away for free distribution and let their fans release them themselves. They release physical media in a variety of special editions and rely on a strong connection with the community. And while you can find their music on Spotify, they're not dependent on it - their fans are willing to pay them directly.

Then of course there's Bandcamp, where a lot of indie artists actively bypass Spotify and rely on direct sales of albums and merchandise. Amanda Palmer of The Dresden Dolls is a great example - she's ditched the label altogether, using crowdfunding (Patreon) and making money by connecting directly with her fans.

And then there are those who actively boycott Spotify. Take Neil Young, who pulled his music from Spotify due to dissatisfaction with the terms and conditions for artists, as well as the controversy surrounding misinformation on the platform. Taylor Swift tried it a few years ago too, pulling her music because she was bothered by how little artists were getting paid - but eventually came back because the pressure was too much.

I think what you're saying is very important - it may not change the mainstream, but if a strong subculture develops that refuses to play by Spotify's rules and offers something different, it may attract more people over time. What do you think might be the trigger? What would it take to get people to start appreciating music again?</div>
    </div>
  </div>

  <div class="message-group">
    <div class="message received">
      <div class="sender">MŽE</div>
      <div class="message-content">Being physically at that artist's concert. There's no other option. As modern technology put people more and more away from physically visiting or spending time with that artist, the relationship to that art becomes more distant and weaker, too.</div>
    </div>
  </div>

  <div class="message-group">
    <div class="message sent">
      <div class="sender">ChatGPT</div>
      <div class="message-content">Yeah, I totally agree. The personal experience is irreplaceable. Once you're at the show, you feel the energy, you see the artist in front of you, you share the moment with other fans - that's something no streaming service or algorithm will ever replace. And that's what builds that real relationship with the music and the artist.

I also think that's what makes the difference between "consuming music" and "experiencing music". Streaming is passive - you put on a playlist and it runs in the background. But when you buy a ticket to a concert, go there, stand in the crowd, experience the atmosphere... you appreciate the artist so much more.

Maybe that's the answer to the question of how to get out of this - if artists want to survive outside of Spotify, they have to build on playing live and getting fans to be willing to pay for just that personal touch.</div>
    </div>
  </div>
</div>

<script>
function switchLanguage(lang) {
  document.getElementById('chat-cs').classList.toggle('active', lang === 'cs');
  document.getElementById('chat-en').classList.toggle('active', lang === 'en');
  document.getElementById('cs-button').classList.toggle('active', lang === 'cs');
  document.getElementById('en-button').classList.toggle('active', lang === 'en');
}
</script>