---
layout: post
title: Shutdown hooks in different runtime environments
date: '2018-10-29 11:53:37 +0100'
date_gmt: '2018-10-29 10:53:37 +0100'
tags:
- programming
---
This article will consider various implementations of a shutdown hook: simply a piece of code we wish to execute at the very end of the application.

The usual scenario of using shutdown hooks is when we want to clean up resources (closing network ports, log files, files in general, streams, …). Normally, we do such as soon as possible but it is immensely useful to have this kind of tool when the user exits the application abruptly (or not in an expected manner). Even if there is no need for a cleanup, it is still sometimes useful to log the fact that the application stopped (and when).

### JVM

First of all, let’s consider the __Java/JVM__ approach to shutdown hook:

{% highlight java %}

public class Main {

    public static void main(String[] args) {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            System.out.println("Doing something really important here...");     
        }));
    }

}
{% endhighlight %}

According to the [Java Core API documentation](https://docs.oracle.com/javase/8/docs/technotes/guides/lang/hook-design.html):

> If the VM crashes due to an error in native code then no guarantee can be made about whether or not the hooks will be run.

In the current implementation, the Java runtime uses an `IdentityHashMap<Thread, Thread>` to store all the threads which will then be executed by the static method `ApplicationShutdownHooks.runHooks()`. I highly suggest to inspect the [source code of the java.lang.ApplicationShutdownHooks class](https://github.com/openjdk/jdk/blob/master/src/java.base/share/classes/java/lang/ApplicationShutdownHooks.java) for more detailed info. The implementation isn't platform dependent (the dependent part is the thread API).

### Bash

{% highlight bash %}
function cleanup() {
    RM=$1
    printf "====TRAP====\nDoing some cleanup...\n"
    # ...
    printf "OK.\n"
    exit 0
}

printf "The script runs here...\n"

trap cleanup EXIT

printf "...and also here.\n"
{% endhighlight %}

In the shell environment, traps are a very vague equivalent of shutdown hooks. The following excerpts are taken from the man page of trap:

{% highlight text %}

trap [-lp] [[arg] sigspec ...]

The command arg is to be read and executed when the shell receives signal(s) sigspec.
If a sigspec is EXIT (0) the command arg is executed on exit from the shell.

{% endhighlight %}
<p>From the man page, we can also learn that the trap command is much more powerful than being a simple <em>shutdown hook executor</em> since each sigspec carries a different behaviour (for example, the trap command can be used for printing out debug messages).</p>

### Python 

{% highlight python %}

import atexit

def first():
    print("Executing the first function.")

def second(_):
    print("Executing the second function.")

def third(a, b, c, d):
    print("Executing the third function.")
    print(len(a + b + c + d))

atexit.register(first)
atexit.register(second, "some argument which will be ignored")
atexit.register(third, "there", "are", "four", "args")

{% endhighlight %}

In Python, the [atexit module](https://docs.python.org/3/library/atexit.html) is used for registering actions which are to be executed before the very termination of the script. The documentation also makes it clear that actions submitted are saved on a LIFO-like structure. The argumentation is that _"lower level modules will normally be imported before higher level modules and thus must be cleaned up later"_.