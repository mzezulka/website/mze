---
layout: post
title: Doučování v roce 2022
date: '2023-01-01 15:21:53 +0100'
date_gmt: '2023-01-01 14:21:53 +0100'
tags:
- maths
---

5.4.2022 jsem odučil svojí první hodinu matematiky.

[Vrhnul jsem se totiž na dráhu](https://www.youtube.com/watch?v=GbSwf7rbwHU){:target="_blank"} lektora ve škole Populo.

Do konce roku jsem odučil přes víc než sto lekcí a v průběhu jsem se naučil spoustů věcí. Třeba i to, jak málo toho vlastně vím. Jednak o lidech, o sobě, tak i o samotné matematice.

Na začátku jsem si taky naivně myslel, že bude velmi dobrý nápad doučovat děti online. _Není._ Ve skutečnosti jakákoliv komunikace na internetu mi silně nevyhovuje, protože mám neustálý pocit, že mi člověk na druhé straně stejně nevěnuje moc pozornosti. Pro mě jsou takové interakce jenom ztrátou času.

Zažil jsem studenty, kteří se pod mým vedením v matematice zlepšili, zažil jsem ale i takové, kteří svého cíle nedosáhli. Pro empatického člověka je ta druhá varianta velmi těžko snesitelná.

V dubnu jsem tak zároveň začal i svůj krysí závod, kdy mám dvě práce. První je na plný úvazek a doučování s proměnlivým počtem odpracovaných hodin, v průměru ale tak okolo šesti až sedmi hodin týdně včetně příprav. Je to dost úmorný životní styl, ale i to byla další věc, kterou jsem si dokázal - totiž že dokážu pracovat i víc než deset hodin denně, a to dlouhodobě. Jestli mi to spíš pomohlo nebo ublížilo, to budu nejspíš schopný posoudit až s větším časovým odstupem.
