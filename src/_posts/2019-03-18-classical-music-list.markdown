---
layout: post
title: Pieces of classical music I've listened to
date: '2019-03-18 05:10:32 +0100'
date_gmt: '2019-03-18 04:10:32 +0100'
tags:
- music
---
Since the middle of March 2019, I've been discovering my deep love of classical music. Since I already don't know what I've listened to so far, I will keep track of all the pieces from now on here.

Composers are sorted by their year of birth.

{% assign composers = site.data.composers | sort: "birth" %}

<ul class="timeline">
{% for composer in composers %}
  <li>
    {% assign loopindex = forloop.index0 | modulo: 2 %}
    {% if loopindex == 0 %}
    <div class="direction-r era-{{ composer.era}}">
    {% else %}
    <div class="direction-l era-{{ composer.era}}">
    {% endif %}
      <div class="flag-wrapper">
        <span class="flag">{{ composer.name }}</span>
        <span class="time-wrapper"><span class="time">{{ composer.birth }} - {{ composer.death }}</span></span>
      </div>
      <div class="desc">
        <ul class="artwork">
          {% for work in composer.works %}
            <li>{{ work | escape }}</li>
          {% endfor %}
        </ul>
      </div>
    </div>
  </li>
{% endfor %}
</ul>