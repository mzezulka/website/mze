---
layout: post
title: Free Culture
date: '2021-01-17 17:34:37 +0100'
date_gmt: '2021-01-17 16:34:37 +0100'
tags:
- music
---

Recently, I’ve read a phenomenal book which was recommended in an equally fantastic video on copyright law, made by David Bruce, a classical music composer and enthusiast. The full name of the book is [Free Culture: How Big Media Uses Technology and the Law to Lock Down Culture and Control Creativity](https://en.wikipedia.org/wiki/Free_Culture_(book)) but I will simply refer to it as __Free Culture__.

<iframe title="Why Copyright Law STEALS From Us All"
        width="640"
        height="360"
        src="https://www.youtube.com/embed/87wHYATdPXs?feature=oembed"
        frameborder="0"
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
        allowfullscreen></iframe>

In Free Culture, Lawrence Lessig provides an insightful and knowledgeable view on many topics related to intellectual property rights, including issues which quite recently arose with introduction of the relatively unregulated and distributed environment of the Internet to the masses, especially issues arising from unlimited file sharing. This short article will refer to a few excerpts from this book. A book released under the Creative Commons License, by the way; I highly recommend reading the book to discover why the author considers CC License to be a great alternative to the standard copyright.

Let us use this first excerpt to define what a free culture actually means:

> A free culture is not a culture without property, just as a free market is not a market in which everything is free. The opposite of a free culture is a "permission culture" - a culture in which creators get to create only with the permission of the powerful, or of creators from the past.

Indeed, __free__ used with such meaning, free as in __gratis__, is not a completely novel concept, as the author continues to describe his main inspiration:

> The inspiration for the title and for much of the argument of this book comes from the work of Richard Stallman and the Free Software Foundation. Indeed, as I reread Stallman's own work, especially the essays in _Free Software, Free Society_, I realized that all of the theoretical insights I develop here are insights Stallman described decades ago.

The most commonplace explanation Richard Stallman uses when debating about the general concept of free software is something along the lines of "free” as in “free speech”, not as in “free beer”[^3]. Advocates of free software sometimes also use the Latin word _libre_ to avoid confusion between the two meanings. What is more, both meanings refer to completely different concepts which are not even mutually exclusive.

To give an example, Steam, a video game digital distribution service, provides a client software to its users which is indeed for free but certainly not _libre_. On the other hand, it is true that the vast majority of _libre_ software is provided for free but it is feasible and sometimes even desirable for libre software users to pay for extra services related to life of software such as software packaging, distribution or its maintenance.

Let us now address some of the interesting points Lessig makes in the book itself.

> A culture without property, or in which creators can't get paid is anarchy, not freedom. Anarchy is not what I advance here.

There are a few dubious passages which I completely disagree with. Conclusions about anarchy and the necessity of having a common legal framework imposed to everyone is one of such passages.
It is quite misleading to say that _"a culture without property is anarchy"_ because the existence of a government (or a lack thereof) is not relevant to property existence or protection in a given society. Natural law exists irrespective of the enacted laws of a state or society and property-related laws are most certainly one of such laws, innate to many of us.

Overall, though, such sections were of marginal importance considering the book quality and most importantly the main topic it covers. The rest of this article refers to ideas I sympathize or even agree with.

> ..., it is the special genius of a common law system, as ours is, that the law adjusts to the technologies of the time. And as it adjusts, it changes. Ideas that were as solid as rock in one age crumble in another.

That is most certainly true not only for law systems but also for the way cultural industry works. With the introduction and ever-increasing use of the Internet, many companies need to adjust the way they deliver content to its customers who are more and more becoming used (and addicted, perhaps) to the comfort Internet technologies provide. One such technology is multimedia streaming. Streaming is one of the most dominant and popular ways of consuming culture today by the Western society [^1] [^2].

The author also suggests that the Western culture (he mostly speaks about US culture but many of his points still apply generally to "the West") is slowly becoming a _permissive culture_, meaning that before manipulating with a work of art, explicit consent from the author must be given.

This indirectly addresses a very important problem arising by this: new artists and people aspiring to make their living out of creating arts are very limited in what they might use. Reasons are manifold: the copyright law is so much complicated and vaguely described that newcomers resign themselves to defending their rights, copyright holder of the work is unknown, etc.

Most importantly, artists do not live in isolation and base their work on influences. Tinkering and playing with previous works of art is a fundamental and irreplaceable way of learning their craft. With permissive culture, we legally prohibit it.

Creating art can be crucial to many people, especially to their freedom of expression. Many school systems completely ignore (or even worse, are not aware of) the fact that each student has a very specific set of abilities. There are many children who express themselves best by speaking in front of an audience, someone prefers writing an essay or creating a presentation. Or a short documentary made out of movie excerpts. With permissive culture and copyright law in place, we selectively destroy such type of creativity.

__No form of expression should be limited on the basis of its potential dangers.__ When you aspire to be a movie producer and have all predispositions to this profession, why should an overwhelmingly protective copyright law endorsed by media corporations such as Disney stand you in the way just because they fear you _might_ use their movies in a way that would be economically harmful to them?

> [...] Any general freedom to build upon the film archive of our culture, a freedom in other contexts presumed for us all, is now a privilege reserved for the funny and famous - and presumably rich. This privilege becomes reserved for two sorts of reasons. The first continues the story of the last chapter: the vagueness of "fair use".  [...] the second reason [is] that the privilege is reserved for the few: The costs of negotiating the legal rights for the creative reuse of content are astronomically high. These costs mirror the costs with fair use: You either pay a lawyer to defend your fair use right or pay a lawyer to track down permissions so you don't have to rely upon fair use rights. Either way, the creative process is a process of paying lawyers - again a privilege, or perhaps a curse, reserved for the few.

_Fair use_ refers to usage of copyrighted work in a way that does not require an explicit permission from its author. You could think of many scenarios where this might be extremely useful __and__ does not go against the copyright holder: education purposes, noncommercial use, works of art which are completely unavailable on the market...

One of the strongest conclusions and arguments against permissive culture is that it destroys liberties of anyone active in the process of art creation since presumption of guilt is directly built into the copyright law: _"you might as well be an artist who manipulates with works of art in a new, creative way, thus adding to the common good, but we're going to persecute you and all the others anyway in case you start doing something we, the state, and already existing corporations consider bad"_. This approach to human rights is extremely dangerous and what is more, potentially formative and influential for further generations.

I will finish this article by giving you a food for thought. Lessig provides a wonderful but frighteningly similar parallel to the way copyright views intellectual property rights:

> Some people use drugs, and I think that's the closest analog, [but] many have noted that the war against drugs has eroded all of our civil liberties because it's treated so many Americans as criminals. Well, I think it's fair to say that file sharing is an order of magnitude larger number of Americans than drug use... If forty to sixty million Americans have become law-breakers, then we're really on a slippery slope to lose a lot of civil liberties for all forty to sixty million of them.

---

[^1]: UMSTEAD, Thomas. Horowitz: Streaming Is the New Normal. _Broadcasting & Cable_ [online]. 12 June 2017 [cit. 2021-01-17]. Available at: https://www.nexttv.com/news/horowitz-streaming-new-normal-166439
[^2]: Comparison of on-demand music streaming services. In: _Wikipedia: the free encyclopedia_ [online]. San Francisco (CA): Wikimedia Foundation, 2001- [cit. 2021-01-17]. Available at: https://en.wikipedia.org/wiki/Comparison_of_on-demand_music_streaming_services
[^3]: What Is Free Software?, Richard Stallman, https://www.gnu.org/philosophy/free-sw.html

