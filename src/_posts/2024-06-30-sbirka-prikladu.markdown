---
layout: post
title: Sbírka úloh z matematiky pro SŠ
date: '2024-06-30 19:09:00 +0200'
date_gmt: '2024-06-30 17:09:00 +0000'
tags:
- maths
---
[Odkaz ke stažení](https://drive.google.com/uc?export=download&id=1iiJ5upMKnItL3ojBNubdxEknhEl3tCU7), pokud se Vám nedaří PDF zobrazit přímo v prohlížeči.
<iframe src="https://drive.google.com/file/d/1iiJ5upMKnItL3ojBNubdxEknhEl3tCU7/preview" width="100%" height="640" type='application/pdf'></iframe>