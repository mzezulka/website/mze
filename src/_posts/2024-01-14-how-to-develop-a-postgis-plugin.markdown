---
layout: post
title: How to Develop a QGIS Plugin
date: '2024-01-14 19:15:01 +0100'
date_gmt: '2024-01-14 18:15:01 +0100'
tags:
- qgis
- gis
- python
- programming
---
During my work at [CleverFarm](https://www.cleverfarm.ag/), I often needed to work with geospatial data managed by Czech farms.

[eAGRI](https://eagri.cz/public/portal/mze/farmar/elektronicka-vymena-dat/prehled-vystavenych-sluzeb), especially its subdivion also referred to as _The Farmer Portal (FP)_ is a critical source of information for many farmers in the Czech Republic in regard to the soil they manage. However, the user interface which FP provides is most often cumbersome to use. [eagris](https://zezulka.gitlab.io/qgis-eagri), a QGIS plugin developed in Python, aims to be a more accessible alternative, open to any changes by anyone who knows a thing or two about programming in Python.

Implementing a QGIS extension seemed like a natural choice for me since many agronomists and other users know how to use this software already.

I started working on the plugin in April 2023 with a basic knowledge of QGIS and Python. After half a year of developing the plugin on my own in my spare time, I managed to create quite a mature plugin with automatic testing CI pipelines which automatically release a new plugin version to the [official QGIS repository](https://plugins.qgis.org/plugins/eagris/#plugin-about) for me.

Unfortunately, there are not that many tutorials on how to develop a custom QGIS plugin from A to Z even though it's relatively an easy thing to do. As of writing this article, there are already hundreds of plugins publicly available for download.

In this series, I would like to provide a step-by-step tutorial on how to develop a plugin on your own:
1. [Plan Your Work](/qgis-plugin-development/1.html)
2. [Setting Up Your Environment for Plugin Development](/qgis-plugin-development/2.html)
3. [Your First Step Towards a QGIS Plugin](/qgis-plugin-development/3.html)
4. [PyQT and Interacting With QGIS](/qgis-plugin-development/4.html)
5. [Geoapify Client Integration Part One](/qgis-plugin-development/5.html)
6. [Geoapify Client Integration Part Two](/qgis-plugin-development/6.html)
7. [Creating and Managing QGIS Vector Layers](/qgis-plugin-development/7.html)
8. [Polishing Up: Documentation, README, CHANGELOG, Release](/qgis-plugin-development/8.html)
