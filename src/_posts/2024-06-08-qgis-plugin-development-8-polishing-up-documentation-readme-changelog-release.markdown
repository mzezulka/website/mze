---
layout: post
title: "8. Polishing Up: Documentation, CHANGELOG, Release"
date: '2024-06-08 18:25:03 +0200'
date_gmt: '2024-06-08 16:25:03 +0200'
tags:
- qgis
- gis
- python
permalink: /qgis-plugin-development/8
---

Once we're done with the first plugin sketch, it's time to tell others how to use it and what its features are and finally - release it!

### Documentation

I won't spend too much time talking about plugin documentation because you can approach it in a few different ways, each with a different level of sophistication:
- write a dead simple `README` and that's it
- create a documentation page using your favourite docs management tool; I highly recommend checking out the [Thyrsis QGIS plugin documentation](https://oslandia.gitlab.io/qgis/thyrsis/en/) which makes use of Sphinx and has wonderful documentation website
- go even a bit fancier and create video tutorials and/or welcome wizards embedded inside QGIS; I sort of implemented the latter option in one of [my other projects](https://gitlab.com/zezulka/qgis-eagri/-/blob/master/eagris/controller/eagriswelcomewizard.py), feel free to take a peek!

### Changelog

If you know your plugin is a long-term project and many features will be probably added in the future, it's a very good idea to keep track of the changes in your project. Not only for the sake of your sanity but also for all your future plugin contributors, too.

For those not in the software development know, [semantic versioning](https://semver.org/) is the industry standard for labeling each new version of your software. The unwritten rule is to start your release at version `0.1.0`.

Your changelog file will then for each version say what changes have been made. I highly recommend using [a well-defined changelog format](https://keepachangelog.com/en/1.1.0/). It should be simple, easy to read and use.

Our changelog could look something like this:

{% highlight text %}

# Changelog

## [0.1.0] - 2024-08-06

### Added

- first development release
- integration of the Geoapify API, especially the Geocoding API
- point vector layer creation based on user prompt
- result table containing detailed query result information

{% endhighlight %}

### Release

So far, we've only installed our plugin locally, i.e. pretty much nobody else can use our software as it has not been published yet.

First of all, you will need to get a new OSGEO user ID if you haven't got one already. Follow the instructions on the [official website](https://plugins.qgis.org/publish/), the process is pretty straightforward.

You then should be able to login to the [QGIS Python Plugin Repository](https://plugins.qgis.org/plugins).

To upload your plugin, you have two options:

Either upload the plugin zip directly from the website, e.g. created by [qgis-plugin-ci](https://opengisch.github.io/qgis-plugin-ci/index.html), or to automate the process a bit, you can write a tiny script based on qgis-plugin-ci which will do it for you. On my Linux machine, I'm using this simple shell script:

{% highlight bash %}

#!/bin/bash
OSGEO_DEFAULT_USERNAME=... # your user ID goes here
PLUGIN_METADATA=... # path to your metadata.txt with 'version' attribute
read -p "Username [${OSGEO_DEFAULT_USERNAME}]: " OSGEO_USER_NAME
read -sp 'Password: ' OSGEO_USER_PASSWORD
RELEASE_VERSION=$(grep 'version=' $PLUGIN_METADATA | sed 's/version=\([0-9.]*\).*/\1/')

OSGEO_USER_NAME=${OSGEO_USER_NAME:-$OSGEO_DEFAULT_USERNAME}

cat << EagrisReleaseInfo

=================================
OSGEO user name: $OSGEO_USER_NAME
release version: $RELEASE_VERSION
=================================
EagrisReleaseInfo

read -p "Do you want to release the plugin? (y/n) " yn

case $yn in
    y|Y|yes )
        qgis-plugin-ci release $RELEASE_VERSION --osgeo-username $OSGEO_USER_NAME --osgeo-password $OSGEO_USER_PASSWORD
	git tag $RELEASE_VERSION
	git push --tags
	echo "Release $RELEASE_VERSION successful.";;
    * ) echo "Release aborted."
esac

{% endhighlight %}

One way or another, the plugin code will then be manually reviewed and after that, the plugin will be available for installation directly from QGIS! If you've marked your plugin as experimental, you will need to [enable experimental plugins in your QGIS](https://docs.qgis.org/latest/en/docs/user_manual/plugins/plugins.html).

This concludes the series. Good luck with your plugin and have fun!
