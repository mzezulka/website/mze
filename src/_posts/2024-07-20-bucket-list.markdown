---
layout: post
title: 🪣List
date: '2024-07-20 16:55:00 +0200'
date_gmt: '2024-07-20 14:55:00 +0000'
tags:
- personal
---
- Become a homeowner
- Try a zipline
- Read all the books written by Bohumil Hrabal
- Sleep at an ice hotel
- Finish writing a math exercises book, including detailed explanation
- Become a geocacher
- Create a web course on programming
- Travel the entire Trans-Siberian Railway
- Visit China