---
layout: post
title: Programming
permalink: /prog/
---
{% for post in site.posts %}
  {% if post.tags contains 'programming' %}
  <article>
    <span color="grey">{{ post.date | date: "%m/%Y" }}</span> <span><a href="{{ post.url }}">{{ post.title }}</a></span>
  </article>
  {% endif %}
{% endfor %}
