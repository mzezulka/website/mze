---
layout: post
title: Personal
permalink: /personal/
---
{% for post in site.posts %}
  {% if post.tags contains 'personal' %}
  <article>
    <span color="grey">{{ post.date | date: "%m/%Y" }}</span> <span><a href="{{ post.url }}">{{ post.title }}</a></span>
  </article>
  {% endif %}
{% endfor %}